from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    technician_id = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.first_name


class Appointment(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    date_time = models.DateTimeField(null=True)
    reason = models.CharField(max_length=200)
    customer = models.CharField(max_length=200)
    vip = models.BooleanField(default=False, null=True)
    status = models.CharField(max_length=200, default="created", null=True, blank=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return self.vin
