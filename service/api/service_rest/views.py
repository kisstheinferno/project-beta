from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import AutomobileVO, Technician, Appointment
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods

class AutomobileVOEnconder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "technician_id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "date_time",
        "reason",
        "customer",
        "vip",
        "status",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"MESSAGE": "Technician could not be created"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_delete_technician(request, id):
    if Technician.objects.filter(id=id).count() > 0:
        technician = Technician.objects.filter(id=id)
        technician.delete()
        return JsonResponse({"message": "Technician deleted successfully"})
    else:
        return JsonResponse(
            {"message": "Invalid technician id"},
            status=400,
        )



@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content=json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"MESSAGE": "Invalid technician id"},
                status=400
            )
        try:
            if AutomobileVO.objects.get(vin=content["vin"]):
                content["vip"] = True
        except AutomobileVO.DoesNotExist:
            content["vip"] = False
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_appointment(request, id):
    if Appointment.objects.filter(id=id).count() > 0:
        appointment = Appointment.objects.filter(id=id)
        appointment.delete()
        return JsonResponse({"message": "Appointment deleted successfully"})
    else:
        return JsonResponse(
            {"message": "Invalid appointment id"},
            status=400,
        )

@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    try:
        content = {"status": "canceled"}
        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid appointment id"},
            status=400,
        )

@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    try:
        content = {"status": "finished"}
        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid appointment id"},
            status=400,
        )
