# Generated by Django 4.0.3 on 2023-12-21 02:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='automobilevo',
            name='sold',
        ),
        migrations.AlterField(
            model_name='appointment',
            name='customer',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='date_time',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='reason',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='status',
            field=models.CharField(default=False, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='technician',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='appointments', to='service_rest.technician'),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='vip',
            field=models.BooleanField(default=False, null=True),
        ),
        migrations.AlterField(
            model_name='technician',
            name='employee_id',
            field=models.PositiveSmallIntegerField(unique=True),
        ),
        migrations.AlterField(
            model_name='technician',
            name='first_name',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='technician',
            name='last_name',
            field=models.CharField(max_length=200),
        ),
    ]
