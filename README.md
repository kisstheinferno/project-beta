# CarCar
CarCar is a car dealership application that manages your inventory, sales and service needs. It allows you to manage your inventory by adding manufacturers and models for easy tracking of new cars. The sales feature allows you to keep track of your customer information, as well as employee information and their sales. The services feature allows you to keep track of your technicians and their upcoming service appointments as well as a comprehensive service history.

Team:

* Adam Fortin - Service
* Dylan Garcia - Sales


## Getting Started

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone https://gitlab.com/kisstheinferno/project-beta

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

## Design
CarCar is made from 3 microservices that interact with each other.

![Diagram](Project-beta-diagram.png)
## Inventory microservice
            Manufacturers Endpoints
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List of manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/
| Get a specfic manufacturer | GET | http://localhost:8100/api/manufacturers/<int:id>
| Update a manufacturer | PUT | http://localhost:8100/api/manufacturers/<int:id>
| Delete a manufacturer | DELETE | http://localhost:8100/api/manufacturers/<int:id>

The manufacturers list will give you a list of all manufacterers. Example of returned code:
~~~
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Toyota"
}
~~~
To create a manufacturer, your POST request must look like the following example:
~~~
{
	"name": "GTA"
}
~~~
To see a specfic manufacturer, send a GET request to http://localhost:8100/api/manufacturers/<int:id> with the "id" field replacing <int:id>. Example of returned code:
~~~
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Honda"
}
~~~
To change a manufacturer, send PUT request to http://localhost:8100/api/manufacturers/<int:id> with the "id" field replacing <int:id> and it must look like the following example:
~~~
{
	"name": "Acura"
}
~~~
To delete a manufacturer, send a DELETE request to http://localhost:8100/api/manufacturers/<int:id> with the "id" field replacing <int:id>.

            Models Endpoints
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List of models | GET | http://localhost:8100/api/models/
| Create a model | POST | http://localhost:8100/api/models/
| Get a specfic model | GET | http://localhost:8100/api/models/<int:id>
| Update a model | PUT | http://localhost:8100/api/models/<int:id>
| Delete a model | DELETE | http://localhost:8100/api/models/<int:id>

The models list will give you a list of all models. Example of returned code:
~~~
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Camry",
	"picture_url": "http://th.bing.com/th/id/OIP.u8aU-F5LUJHb4yzzqPRPFgAAAA?rs=1&pid=ImgDetMain",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Toyota"
	}
}
~~~
To create a model, your POST request must look like the following example:
~~~
{
			"name": "MDX",
			"picture_url": "https://hips.hearstapps.com/hmg-prod/images/2022-acura-mdx-type-s-comparison-101-1661794292.jpg?crop=0.637xw:0.716xh;0.192xw,0.186xh&resize=768:*",
			"manufacturer_id": 7
}
~~~
To see a specfic model, send a GET request to http://localhost:8100/api/models/<int:id> with the "id" field replacing <int:id>. Example of returned code:
~~~
{
	"href": "/api/models/4/",
	"id": 4,
	"name": "MDX",
	"picture_url": "https://hips.hearstapps.com/hmg-prod/images/2022-acura-mdx-type-s-comparison-101-1661794292.jpg?crop=0.637xw:0.716xh;0.192xw,0.186xh&resize=768:*",
	"manufacturer": {
		"href": "/api/manufacturers/7/",
		"id": 7,
		"name": "Acura"
	}
}
~~~
To change a model, send PUT request to http://localhost:8100/api/models/<int:id> with the "id" field replacing <int:id> and it must look like the following example(it is not possible to update the manufacturer):
~~~
{
			"name": "RDX",
			"picture_url": "https://vehicle-images.dealerinspire.com/b443-11000237/5J8TC2H87RL000388/23f6bee285d61fb43d1f0f69411d7dce.jpg",
}
~~~
To delete a model, send a DELETE request to http://localhost:8100/api/models/<int:id> with the "id" field replacing <int:id>.

            Automobiles Endpoints
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List of automobiles | GET | http://localhost:8100/api/automobiles/
| Create a automobile | POST | http://localhost:8100/api/automobiles/
| Get a specfic automobile | GET | http://localhost:8100/api/automobiles/:vin
| Update a automobile | PUT | http://localhost:8100/api/automobiles/:vin
| Delete a automobile | DELETE | http://localhost:8100/api/automobiles/:vin

The automobiles list will give you a list of all automobiles. Example of returned code:
~~~
{
	"href": "/api/automobiles/94393434233242764/",
	"id": 3,
	"color": "Red",
	"year": 2011,
	"vin": "94393434233242764",
	"model": {
		"href": "/api/models/3/",
		"id": 3,
		"name": "Camaro",
		"picture_url": "https://www.motortrend.com/uploads/sites/10/2015/11/2011-chevrolet-camaro-6.2-1ss-coupe-angular-front.png",
		"manufacturer": {
			"href": "/api/manufacturers/4/",
			"id": 4,
			"name": "Chevrolet"
		}
	},
	"sold": false
}
~~~
To create an automobile, your POST request must look like the following example:
~~~
{
	"color": "Black",
	"year": 2024,
	"vin": "00000222223333344",
	"model_id": 4
}
~~~
To see a specfic automobile, send a GET request to http://localhost:8100/api/automobiles/:vin with the "vin" field replacing :vin. Example of returned code:
~~~
{
	"href": "/api/automobiles/00000222223333344/",
	"id": 4,
	"color": "Black",
	"year": 2024,
	"vin": "00000222223333344",
	"model": {
		"href": "/api/models/4/",
		"id": 4,
		"name": "RDX",
		"picture_url": "https://vehicle-images.dealerinspire.com/b443-11000237/5J8TC2H87RL000388/23f6bee285d61fb43d1f0f69411d7dce.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/7/",
			"id": 7,
			"name": "Acura"
		}
	},
	"sold": false
}
~~~
To change an automobile, send PUT request to http://localhost:8100/api/automobiles/:vin with the "vin" field replacing :vin and it must look like the following example:
~~~
{
	"color": "Grey",
	"year": 2024,
	"sold": true
}
~~~
To delete an automobile, send a DELETE request to http://localhost:8100/api/automobiles/:vin with the "vin" field replacing :vin.

## Service microservice
            Service Endpoints
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List of technicians | GET | http://localhost:8080/api/technicians/
| Create a technicians | POST | http://localhost:8080/api/technicians/
| Deletea a technicians | DELETE | http://localhost:8080/api/technicians/<int:id>
| Service appointment history | GET | http://localhost:8080/api/appointments/
| Create a service appointment | POST | http://localhost:8080/api/appointments/
| Delete a service appointment | DELETE | http://localhost:8080/api/appointments/<int:id>
| Set an appointment to "canceled" | PUT | http://localhost:8080/api/appointments/<int:id>/cancel/
| Set an appointment to "finished" | PUT | http://localhost:8080/api/appointments/<int:id>/finish/

The technicians list will give you a list of all current technicians. Example of returned data:
~~~
{
	"id": 2,
	"first_name": "Sum",
    "last_name": "Ting Wong",
	"technician_id": 3
},
~~~
To create a technician, your post request must look like this example:
~~~
{
    "first_name": "Wei",
    "last_name": "Fa Ling",
    "technician_id": 5
}
~~~
To deleted a sqecific technician, make a DELETE request to http://localhost:8080/api/technicians/<int:id> with "id" field replacing <int:id>. The "id" and "technician_id" are 2 seperate fields, do not make the mistake of using the technician ID when trying to delete, as you may delete the wrong technician.

The appointments list will give a list of all appointments, current, canceled, or finished. Example of returned data:
~~~
{
	"id": 6,
	"vin": "34252344251314234",
	"date_time": "2023-12-30T23:44:00+00:00",
	"reason": "Hope restored",
	"customer": "Almost Gotit",
	"vip": false,
	"status": "created",
	"technician": {
	    "id": 6,
		"first_name": "Wi ",
		"last_name": "Tu Lo",
		"technician_id": 4
	}
}
~~~
To create an appointment, your POST request must look like the following example:
~~~
{
	"vin": "11111111111111111",
	"date_time": "2024-1-03T11:42:00+00:00",
	"reason": "Almost There",
	"customer": "Inches Away",
	"vip": false,
	"status": "created",
	"technician": 3
}
~~~
To delete a specfic appointment, make a DELETE request to http://localhost:8080/api/appointments/<int:id> with the "id" field replacing <int:id>.

To set an appointment to canceled, make a PUT request to http://localhost:8080/api/appointments/<int:id>/cancel/ with the "id" field replacing <int:id>. Example of the returned data:
~~~
{
	"id": 6,
	"vin": "34252344251314234",
	"date_time": "2023-12-30T23:44:00+00:00",
	"reason": "Hope restored",
	"customer": "Almost Gotit",
	"vip": false,
	"status": "canceled",
	"technician": {
		"id": 6,
		"first_name": "Wi ",
		"last_name": "Tu Lo",
		"technician_id": 4
	}
}
~~~
To set an appointment to finished, make a PUT request to http://localhost:8080/api/appointments/<int:id>/finish/ with the "id" field replacing <int:id>. Example of the returned data:
~~~
{
	"id": 3,
	"vin": "12345678901234567",
	"date_time": "2023-12-31T22:26:00+00:00",
	"reason": "Questioning my life choices",
	"customer": "Brain Hurts",
	"vip": false,
	"status": "finished",
	"technician": {
		"id": 2,
		"first_name": "Sum",
		"last_name": "Ting Wong",
		"technician_id": 3
	}
},
~~~

## Sales microservice

        Sales Endpoints
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List of salespeople | GET | http://localhost:8090/api/salespeople/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8080/api/salespeople/<int:id>
| List of customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Delete a customer | DELETE | http://localhost:8090/api/customers/<int:id>
| List of sales | GET | http://localhost:8090/api/sales/
| Create a sale | POST | http://localhost:8090/api/sales/
| Delete a sale | DELETE | http://localhost:8090/api/sales/<int:id>

The salesperson list will give you a list of all current salespeople. Example of returned data:
~~~
{
	"href": "/api/salespeople/1/",
	"first_name": "Last",
	"last_name": "Day",
	"employee_id": "lday",
	"id": 1
}
~~~
To create a salesperson, your post request must look like this example:
~~~
{
	"first_name": "Not",
	"last_name": "Done",
	"employee_id": "yiks"
}
~~~
To delete a specfic salesperson, make a DELETE request to http://localhost:8090/api/salespeople/<int:id> with the "id" field replacing <int:id>.

The customers list will give you a list of all current customers. Example of returned data:
~~~
{
	"href": "/api/customers/1/",
	"first_name": "Sherlock",
	"last_name": "Holmes",
	"address": "220 Bakers treet",
	"phone_number": 18675309,
	"id": 1
}
~~~
To create a customer, your post request must look like this example:
~~~
{
    "first_name": "Wei",
    "last_name": "Fa Ling",
    "address": "220 Bakers treet",
	"phone_number": 18675309,
}
~~~
To delete a specfic customer, make a DELETE request to http://localhost:8090/api/customers/<int:id> with the "id" field replacing <int:id>.

The sales list will give you a list of all current sales. Example of returned data:
~~~
{
	"salesperson": {
		"href": "/api/salespeople/1/",
		"first_name": "danny",
		"last_name": "cantu",
		"employee_id": "4",
		"id": 1
	},
	"customer": "bob dylan",
	"automobile": {
		"vin": "1C3CC5FB2AN120174",
		"sold": true,
		"id": 1
	},
	"price": "123.00",
	"id": 38
},
~~~
To create a sale(salesperson and customer must be the "id" field for their respective values), your post request must look like this example:
~~~
{
	"price": 123.56,
	"automobile": "1C3CC5FB2AN120172",
	"salesperson": 2,
	"customer": 2
}
~~~
To delete a specfic sale, make a DELETE request to http://localhost:8090/api/sales/<int:id> with the "id" field replacing <int:id>.
