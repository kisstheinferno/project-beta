import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutomobileForm from './inventory/AutomobileForm';
import AutomobileList from './inventory/AutomobileList';
import ModelsForm from './inventory/ModelsForm';
import ModelsList from './inventory/ModelsList';
import AppointmentForm from './service/AppointmentForm';
import AppointmentList from './service/AppointmentList';
import ServiceHistory from './service/ServiceHistory';
import TechnicianForm from './service/TechnicianForm';
import TechnicianList from './service/TechnicianList';
import ManufacturersList from './inventory/ManufacturersList';
import ManufacturerForm from './inventory/ManufacturerForm';
import SalesPersonForm from './sales/SalesPersonForm'
import SalesPeopleList from './sales/SalesPeopleList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SalesForm from './sales/SalesForm';
import SalesList from './sales/SalesList';
import SalesPeopleHistory from './sales/SalesPeopleHistory';

function App() {

  const [manufacturers, setManufacturers ] = useState([]);
  const [models, setModels] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [technicians, setTechnicians] = useState([]);
  const [automobiles, setAutomobiles] = useState([])

  async function getManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }
  async function getAutomobiles() {
    const response = await fetch("http://localhost:8100/api/automobiles/")
    const { autos } = await response.json()
    setAutomobiles(autos)
  }
  async function getModels() {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }
  async function getTechnicians() {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }
  async function getAppointments() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  }


  useEffect(() => {
    getModels();
    getManufacturers();
    getTechnicians();
    getAppointments();
    getAutomobiles();



  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians/">
            <Route index element={<TechnicianList technicians={technicians} />} />
            <Route path="create/" element={<TechnicianForm getTechnicians={getTechnicians} />} />
          </Route>
          <Route path="/appointments/">
            <Route index element={<AppointmentList appointments={appointments} getAppointments={getAppointments} />} />
            <Route path="create/" element={<AppointmentForm technicians={technicians} getAppointments={getAppointments} />} />
            <Route path="history/" element={<ServiceHistory appointments={appointments} />} />
          </Route>
          <Route path="/models/">
            <Route index element={<ModelsList models={models} />} />
            <Route path="create/" element={<ModelsForm manufacturers={manufacturers} getModels={getModels} />} />
          </Route>
          <Route path="/automobiles/">
            <Route index element={<AutomobileList automobiles={automobiles} />}/>
            <Route path="create/" element={<AutomobileForm models={models} getAutomobiles={getAutomobiles} />}/>
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="create/" element={<ManufacturerForm />} />
          </Route>
          <Route path="salespeople">
            <Route path="new" element={<SalesPersonForm />} />
            <Route index element={<SalesPeopleList />} />
            <Route path="history" element={<SalesPeopleHistory />} />
          </Route>
          <Route path="customers">
            <Route path="new" element={<CustomerForm />} />
            <Route index element={<CustomerList />} />
          </Route>
          <Route path="sales">
            <Route path="new" element={<SalesForm />} />
            <Route index element={<SalesList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );

}

export default App;
