import { useEffect, useState } from 'react';

function SalesPeopleList() {
  const [salespeople, setSales] = useState([])

  const getData = async () => {
    const url = 'http://localhost:8090/api/salespeople/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      setSales(data.salespeople)
    }
  }

  useEffect(()=>{
    getData()
  }, [])
  
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {salespeople.map(sales => {
          return (
            <tr key={sales.href}>
              <td>{ sales.employee_id }</td>
              <td>{ sales.first_name }</td>
              <td>{ sales.last_name }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default SalesPeopleList;
