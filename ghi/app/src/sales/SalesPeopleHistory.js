import { useEffect, useState } from 'react';

function SalesPeopleHistory() {
  const [salespeople, setSales] = useState([])
  const [salesData, setSalesData] = useState([])

  const getDataSalespeople = async () => {
    const url = 'http://localhost:8090/api/salespeople/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      setSales(data.salespeople)
    }
  }


  const getSalesData = async (value) => {
    const url = 'http://localhost:8090/api/sales/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      const soldHistory = data.sales.filter(
        (sales) => sales.salesperson.employee_id === value
      )
      setSalesData(soldHistory)
      
    }
  }
  

  useEffect(()=>{
    getSalesData()
    getDataSalespeople()
  }, [])

  const handleFormChange = (e) => {
    const value = e.target.value;
    getSalesData(value)
   }
  
  return (
    <div className="container mt-3">
        <div>
            <h1>Salesperson History</h1>
            <select onChange={handleFormChange}>
                <option value="">Select Salesperson</option>
                {salespeople.map(salesP => {
                    return (
                        <option key={salesP.employee_id} value={salesP.employee_id}>{salesP.first_name} {salesP.last_name}</option>
                    )
                    })}
            </select>
        </div>
    <table className="table table-striped">
      <thead>
        <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {salesData.map(sale => {
            return (
            <tr key={sale.id}>
                <td>{ sale.salesperson.first_name } {sale.salesperson.last_name}</td>
                <td>{ sale.customer }</td>
                <td>{ sale.automobile.vin }</td>
                <td>${ sale.price }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </div>
  );
}

export default SalesPeopleHistory;
