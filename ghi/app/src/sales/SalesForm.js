import React, { useEffect, useState } from 'react';

function SalesForm() {
  const [autos, setAutos] = useState([])
  const [salespeople, setSalespeople] = useState([])
  const [customers, setCustomers] = useState([])

  const [formData, setFormData] = useState({
    price: '',
    automobile:'',
    salesperson:'',
    customer:''
  })
  
  const fetchDataAutos = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const soldAutos = data.autos.filter(
        (autos) => autos.sold !== true
      )
      setAutos(soldAutos);
    }
  }

  const fetchDataSalespeople = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  }

  const fetchDataCustomers = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }

  useEffect(() => {
    fetchDataAutos()
    fetchDataSalespeople()
    fetchDataCustomers()
  }, []);


  const handleSubmit = async (event) => {
    
    event.preventDefault();
    const url = 'http://localhost:8090/api/sales/'

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };


    
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setFormData({
        price:'',
        automobile:'',
        salesperson:'',
        customer:'',
      });

      fetch(`http://localhost:8100/api/automobiles/${formData.automobile}/`, {
        method: "PUT",
        headers: {
        "Content-Type": "application/json"
        },
        body: JSON.stringify({
        sold: true
        })
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="my-5 container">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="mb-3">
                <select value={formData.automobile} onChange={handleFormChange} required name="automobile" id="automobile" className="form-select">
                    <option value="">Automobile VIN</option>
                    {autos.map(automobile => {
                    return (
                        <option key={automobile.vin} value={automobile.vin} id={automobile.id}>{automobile.vin}</option>
                    )
                    })}
                </select>
            </div>

            <div className="mb-3">
                <select value={formData.salesperson} onChange={handleFormChange} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Salesperson</option>
                    {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                    )
                    })}
                </select>
            </div>

            <div className="mb-3">
                <select value={formData.customer} onChange={handleFormChange} required name="customer" id="customer" className="form-select">
                    <option value="">Customer</option>
                    {customers.map(customer => {
                    return (
                        <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                    )
                    })}
                </select>
            </div>

            <div className="form-floating mb-3">
              <input value={formData.price} onChange={handleFormChange} placeholder="price" required type="number" step="0.01" name="price" id="price" className="form-control" />
              <label htmlFor="name">Price</label>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
      </div>
  )
}

export default SalesForm;
