import React, { useState } from 'react';

function ModelsForm(props) {
    const [name, setName] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const [manufacturer, setManufacturer] = useState("");

    function handleNameChange(event) {
        const { value } = event.target;
        setName(value);
    }
    function handlePictureUrlChange(event) {
        const { value } = event.target;
        setPictureUrl(value);
    }
    function handleManufacturerChange(event) {
        const { value } = event.target;
        setManufacturer(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturer;
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch("http://localhost:8100/api/models/", fetchConfig);
        if (response.ok) {
            setName("");
            setPictureUrl("");
            setManufacturer("");
            props.getData();
        }


    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a vehicle model</h1>
                    <form onSubmit={handleSubmit} id="add-model-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} required type="text" id="name" className="form-control"/>
                            <label htmlFor="name">Model name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} value={pictureUrl} required type="text" id="pictureUrl" className="form-control"/>
                            <label htmlFor="pictureUrl">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleManufacturerChange} value={manufacturer} required className="form-select" id="manufacturer">
                                <option value="">Choose a manufacturer</option>
                                {props.manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default ModelsForm;
