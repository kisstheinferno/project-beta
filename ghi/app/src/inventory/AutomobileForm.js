import React, { useState, useEffect } from 'react';

const AutomobileForm = () => {
    const [vin, setVin] = useState("");
    const [color, setColor] = useState("");
    const [models, setModelsData] = useState([]);
    const [year, setYear] = useState("");
    const [model_id, setModel] = useState("")

    function handleVinChange(event) {
        const { value } = event.target;
        setVin(value);
    }
    function handleColorChange(event) {
        const { value } = event.target;
        setColor(value);
    }
    function handleYearChange(event) {
        const { value } = event.target;
        setYear(value);
    }
    function handleModelChange(event) {
        const { value } = event.target;
        setModel(value);
    }
    async function getModelData() {
         const response = await fetch("http://localhost:8100/api/models/");
         if (response.ok) {
             const data = await response.json();
             setModelsData(data.models);
         }
     }

    useEffect(() => {
        getModelData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();
        const automobile = {};
        automobile.vin = vin;
        automobile.color = color;
        automobile.year = year;
        automobile.model_id = model_id;
        const fetchConfig = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(automobile)
        };
        const response = await fetch("http://localhost:8100/api/automobiles/", fetchConfig);
        if (response.ok) {
            setVin("");
            setColor("");
            setModel("");
            setYear("");
        }
    };

    return (
                <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Automobile</h1>
                        <form onSubmit={handleSubmit} id="create-automobile-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleVinChange} value={vin} required type="text" id="vin" className="form-control"/>
                                <label htmlFor="vin">Vin</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleColorChange} value={color} required type="text" id="color" className="form-control"/>
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleYearChange} value={year} required type="text" id="year" className="form-control"/>
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="mb-3">
                            <select onChange={handleModelChange} value={model_id} required className="form-select" id="model_id">
                                <option value="">Choose a model</option>
                                {models.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>{model.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                            <button type="submit" className="btn btn-primary">Add Automobile</button>
                        </form>
                    </div>
                </div>
            </div>
    );
};

export default AutomobileForm;
