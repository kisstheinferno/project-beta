import React, { useState, useEffect } from "react";

function AppointmentsList() {
    const [appointments, setAppointments] = useState([])
    const [setTechnicians] = useState([])

    const getAppointment = async () => {
        const response = await fetch("http://localhost:8080/api/appointments/")
        if (response.ok) {
            const data = await response.json()
            const activeAppointments = data.appointments.filter(
                (appointment) => appointment.status !== "cancelled" && appointment.status !== "finished"
            );
            setAppointments(activeAppointments)
        }
    }
    const getTechnician = async () => {
        const response = await fetch("http://localhost:8080/api/technicians/")
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }

    const handleCancel = async (id) => {
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({ status: "Cancelled" }),
            headers: {
                "Content-Type": "application/json",
            },
        }
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, fetchConfig)
        if (response.ok) {
            setAppointments((appointments) =>
                appointments.filter((appointment) => appointment.id !== id)
            )
        }
    }

    const handleFinish = async (id) => {
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({ status: "Finished" }),
            headers: {
                "Content-Type": "application/json",
            },
        }
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, fetchConfig)
        if (response.ok) {
            setAppointments((appointments) =>
                appointments.filter((appointment) => appointment.id !== id)
            )
        }
    }

    useEffect(() => {
        getAppointment();
        getTechnician();
    }, [])

    return (
    <div>
        <h1>Appointments</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Vin</th>
                    <th>Date/Time</th>
                    <th>Reason</th>
                    <th>Customer</th>
                    <th>VIP</th>
                    <th>Technician</th>
                    <th>Status</th>
                </tr>
            </thead>
        <tbody>
                {appointments.map((appointment) => {
                        return (
                    <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.date_time}</td>
                    <td>{appointment.reason}</td>
                    <td>{appointment.customer}</td>
                    <td>{appointment.vip ? "Yes" : "No"}</td>
                    <td>{appointment.technician.first_name + " " + appointment.technician.last_name}</td>
                    <td><button onClick={() => handleCancel(appointment.id)}className="btn btn-danger">Cancel</button></td>
                    <td><button onClick={() => handleFinish(appointment.id)} className="btn btn-primary"> Finish</button></td>
                </tr>
                );
            })}
        </tbody>
        </table>
        </div>
    );
}

export default AppointmentsList;
