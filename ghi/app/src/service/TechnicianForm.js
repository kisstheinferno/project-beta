import React, { useState } from 'react';

function TechnicianForm() {
    const [first_name, setFirstName] = useState("")
    const [last_name, setLastName] = useState("")
    const [technician_id, setTechnician_id] = useState("")

    const handleTechnician_idChange = (event) => {
        const value = event.target.value
        setTechnician_id(value)
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.technician_id = technician_id;
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch("http://localhost:8080/api/technicians/", fetchConfig)
        if (response.ok){
            setFirstName("")
            setLastName("")
            setTechnician_id("")
        }
    }

    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new technician</h1>
            <form onSubmit={handleSubmit} id="createTechnicianForm">
              <div className="form-floating mb-3">
                <input onChange={handleFirstNameChange} value={first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control"/>
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleLastNameChange} value={last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control"/>
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTechnician_idChange} value={technician_id} placeholder="Address" required type="text" name="technician_id" id="technician_id" className="form-control"/>
                <label htmlFor="technician_id">Employee ID</label>
              </div>
              <button className="btn btn-primary">Create </button>
              </form>
          </div>
        </div>
      </div>
        </>
    )
}
export default TechnicianForm;
