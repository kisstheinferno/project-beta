# Generated by Django 4.0.3 on 2023-12-21 18:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0003_customer_phone_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salesperson',
            name='employee_id',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
