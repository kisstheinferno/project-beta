from common.json import ModelEncoder
from .models import AutomobileVO, Sale, SalesPerson, Customer


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id"
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id"
    ]

class SalesListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "salesperson",
        "customer",
        "automobile",
        "price",
        "id"
    ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalesPersonEncoder(),
    }
    def get_extra_data(self, o):
        return {
            "customer": o.customer.first_name + " " + o.customer.last_name,
        }


class SalesDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobile",
        "customer",
        "salesperson",
    ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
    }
    def get_extra_data(self, o):
        return {
            "customer": o.customer.id,
            "salesperson": o.salesperson.id
        }
