from django.urls import path 
from .views import list_sales_people, show_sales_person, list_customers, show_customer, list_sales, show_sales


urlpatterns = [
    path("salespeople/", list_sales_people, name="create_sales_people"),
    path("salespeople/<int:pk>/", show_sales_person, name="show_sales_person"),
    path("customers/", list_customers, name="create_customer"),
    path("customers/<int:pk>/", show_customer, name="show_customer"),
    path("sales/", list_sales, name="create_sales"),
    path("sales/<int:pk>/", show_sales, name="show_sales"),
]
